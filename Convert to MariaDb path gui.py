
from tkinter import *

import subprocess

def clear_entry_fields():
   #print("Input: %s\nOutput: %s" % (e1.get(), e2.get()))
   e1.delete(0,END)
   e2.delete(0,END)

def fix_win_2_mariadb_path():
   e2.delete(0,END)
   str_in1 = e1.get()
   str_in1 = str_in1.replace('\\','/')
   #print(special)
   e2.insert(0,str_in1)
   #also place the output on the clipboard ready for paste access
   print(e2.get())
   copy2clip(e2.get())
   #dev trying to get feedback to print to label part 1/2
   #master.label3text.set('mmm')
   #label3.config(text='tttt')
   #label3. = "result on clipboard"
   #dev ends

def fix_mariadb_2_win_path():
   e2.delete(0,END)
   str_in2 = e1.get()
   str_in2 = str_in2.replace('/','\\')
   #print(special)
   e2.insert(0,str_in2)
   #also place the output on the clipboard ready for paste access
   copy2clip(e2.get())

#Copy a "fixed" path string to the clipboard.
def copy2clip(txt):
	#was:
	#cmd='echo '+txt.strip()+'|clip'
	#now:
	cmd='echo '+txt.strip()+'|clip'
	#the following does not seem to work when constructed
	#and passed via Python
	#echo|set /p="Hello World"
	print(cmd)
	return subprocess.check_call(cmd, shell=True)
   
master = Tk()
Label(master, text="Enter Path").grid(row=0)
Label(master, text="Result Path").grid(row=1)

e1 = Entry(master)
Button(master, text='Fix to MariaDb', command=fix_win_2_mariadb_path).grid(row=0, column=2, sticky=W, pady=4)
e2 = Entry(master)

#place the entry fields
e1.grid(row=0, column=1, ipadx=180, ipady=4)
e2.grid(row=1, column=1, ipadx=180, ipady=4)

#command=master.quit
Button(master, text='Quit', command=master.destroy).grid(row=3, column=0, sticky=W, pady=4)
Button(master, text='Clear',
       command=clear_entry_fields).grid(row=1, column=2, sticky=W, pady=4)

#dev trying to get feedback to print to label part 2/2
#master.label3text = 'ttt'
#label3 = Label(master, text=master.label3text).grid(row=3, column=1)
#label3.pack()
  #dev ends
	   
Button(master, text='Fix to Windows', command=fix_mariadb_2_win_path).grid(row=4, column=0, sticky=W, pady=4)

mainloop()
