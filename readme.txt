
## Convert to MariaDb path gui
## CMPgui

# Changes:

initial version 		05/10/2022	macairney@mailfence.com
MariaDb replaces MySQL		16/01/2024	macairney@mailfence.com

# Intro:

Text conversion graphical user interface 
for use in the Microsoft Windows operating system

Useful for changing a Windows folder path into a MariaDb compatible folder path

"Fix to MariaDb" button; replaces back slashes with forward slashes and place resulting text into Clipboard ready to paste in some other app
"Fix to Windows" button; replaces forward slashes with back slashes and place resulting text into Clipboard ready to paste in some other app


# Example Use:

Enter Path			C:\work\prog\python3\reversi	
Result Path & Clipboard		C:/work/prog/python3/reversi


# Deployment info:

Install Python 3
Modify "Convert to MariaDb path gui.bat" to suit Python installed version folder name and path to program

pipreqs C:\work\prog\Python3\reversi
zero import libraries required


# Tested successfully in:

Windows 8.1
Windows 10









